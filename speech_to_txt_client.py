#!/usr/bin/env python
#copy write (c) 2018,willow grage, Inc.
#All rights reserved 
#Copyright <2018> <thoufeeque s>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#python 2.x program for sentiment analysis.

import sys
import rospy
from sentiment_analysis_2.srv import *
import speech_recognition as sr

def speech_to_text_client(a):
    rospy.wait_for_service('analysis')
    try:
#create a handle for calling the service:
        analysis = rospy.ServiceProxy('analysis', Sentiment)
        resp1 = analysis(a)
        return resp1.analysis
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


# main function
if __name__ == "__main__":
	# Record Audio
	r = sr.Recognizer()
    	with sr.Microphone() as source:
    		audio = r.listen(source)
#store the audio in to a variable.
    	a=r.recognize_google(audio)
	print speech_to_text_client(a)
